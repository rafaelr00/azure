# Atlassian Bitbucket Data Center

Bitbucket Data Center gives you uninterrupted access to Bitbucket with performance at scale, disaster recovery and instant scalability when hosting our applications in your Azure private cloud account.

## Deploy to Azure Portal

[![Deploy Bitbucket Data Center to Azure Portal](https://azuredeploy.net/deploybutton.png)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fbitbucket.org%2Frafaelr00%2Fazure%2Fraw%2F7ac3c7fcfa39ac675b7d99e884304dc9c4cc0359%2Fatlassian-azure-deployment%2Fbitbucket%2FmainTemplate.json)


## View Azure Deployment Results

View deployment output values in Azure Portal for endpoints, DB url etc.  
![Bitbucket Deployment Results](images/BitbucketDeploymentResults.png "Bitbucket Deployment Results")

## Development
Please see the [DEVELOPING.md](../DEVELOPING.md) for more information on how you can use the update/develop the templates.
